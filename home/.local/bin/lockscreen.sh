#!/usr/bin/env dash

LOCK="/tmp/lockscreen.png"
ICON="${XDG_STATE_HOME}/lock_icon"

rm "${LOCK}"

scrot "${LOCK}"
convert "${LOCK}" -scale 10% -scale 1000% -fill black -colorize 65% -colorspace srgb "${LOCK}"
convert "${LOCK}" "${ICON}" -gravity center -composite -matte "${LOCK}"
i3lock -e -i "${LOCK}"
