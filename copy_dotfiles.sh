#!/usr/bin/env dash

init_dir() {
	dir="${1}"

	if ! [ -d "${dir}" ]
	then
		mkdir -p "${dir}"
	else
		rm -rf --interactive=never "${dir:?}/"*
	fi
}

make_dir() {
	dir="${1}"

	if ! [ -d "${dir}" ]
	then
		mkdir -p "${dir}"
	fi
}

git_submodule() {
	repo="${1}"
	location="${2}"

	if [ -d "${location}" ]
	then
		git -C "${location}" pull
	else
		git submodule add "${repo}" "${location}"
	fi
}

git_commit() {
	message="${1}"

	git add .
	git status

	if [ -n "${message}" ]
	then
		git commit -m "${message}"
	else
		git commit -m "$(date --rfc-3339=seconds)"
	fi
}

init_dir "./boot"
cp 	"/boot/config-"*"-gentoo-gaming" \
	"/boot/config-"*"-gentoo-hardened" \
	"./boot/"

init_dir "./etc"
cp -rf 	"/etc/local.d" \
 	"/etc/portage" \
 	"/etc/udev" \
 	"/etc/sysctl.conf" \
 	"/etc/fstab" \
 	"/etc/environment" \
	"./etc/"

init_dir "./etc/profile.d"
cp "/etc/profile.d/xdg_base_directory.sh" "./etc/profile.d/xdg_base_directory.sh"

make_dir "./etc/default"
cp "/etc/default/grub" "./etc/default/grub"

make_dir "./etc/security"
cp "/etc/security/limits.conf" "./etc/security/limits.conf"

init_dir "./var"
make_dir "./var/lib/portage"
cp "/var/lib/portage/world" "./var/lib/portage/"

make_dir "./home"
cp "/home/${USER}/".*rc "./home/"

make_dir "./home/.config"
cp -rf	"/home/${USER}/.config/conky" \
	"/home/${USER}/.config/mpv" \
	"/home/${USER}/.config/pulse" \
	"/home/${USER}/.config/alsa" \
	"./home/.config/"

init_dir "./home/.local/bin"
cp	"/home/${USER}/.local/bin/diffcd" \
	"/home/${USER}/.local/bin/lockscreen.sh" \
	"/home/${USER}/.local/bin/runasanother" \
	"/home/${USER}/.local/bin/smartresize" \
	"/home/${USER}/.local/bin/mesa_"* \
	"./home/.local/bin/"

git_submodule "https://github.com/ohmyzsh/ohmyzsh.git" "./home/.oh-my-zsh"

make_dir "./home/.vim/bundle"
git_submodule "https://github.com/VundleVim/Vundle.vim.git" "./home/.vim/bundle/Vundle.vim"
git_submodule "https://github.com/vim-airline/vim-airline.git" "./home/.vim/bundle/vim-airline"
git_submodule "https://github.com/AnthonyDiGirolamo/airline-themes.git" "./home/.vim/bundle/vim-airline-themes"
git_submodule "https://github.com/dracula/vim.git" "./home/.vim/bundle/dracula"
cp "/home/${USER}/.vim/vimrc" "./home/.vim/vimrc"

make_dir "./home/zsh/custom/plugins"
make_dir "./home/zsh/custom/themes"
git_submodule "https://github.com/zsh-users/zsh-autosuggestions" "./home/zsh/custom/plugins/zsh-autosuggestions"
git_submodule "https://github.com/romkatv/powerlevel10k.git" "./home/zsh/custom/themes/powerlevel10k"

git_commit "${1}"
