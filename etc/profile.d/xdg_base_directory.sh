if [ "${USER}" ]
then
	export XDG_CACHE_HOME="/tmp/${USER}/.cache"
fi

if [ "${HOME}" ]
then
	export XDG_CONFIG_HOME="${HOME}/.config"
	export XDG_DATA_HOME="${HOME}/.local/share"
	export XDG_STATE_HOME="${HOME}/.local/state"

	# Workarounds
	export DOCKER_CONFIG="${XDG_CONFIG_HOME}/docker"

	export GNUPGHOME="${XDG_DATA_HOME}/gnupg"

	export GOPATH="${XDG_DATA_HOME}/go"

	export GRADLE_USER_HOME="${XDG_DATA_HOME}/gradle"
	export _JAVA_OPTIONS="${_JAVA_OPTIONS} -Djava.util.prefs.userRoot=${XDG_CONFIG_HOME}/java"

	export KDEHOME="${XDG_CONFIG_HOME}/kde"
	export DVDCSS_CACHE="${XDG_DATA_HOME}/dvdcss"

	export NODE_REPL_HISTORY="${XDG_DATA_HOME}/node_repl_history"
	export NPM_CONFIG_USERCONFIG="${XDG_CONFIG_HOME}/npm/npmrc"

	export NUGET_PACKAGES="${XDG_STATE_HOME}/NuGetPackages"

	export PARALLEL_HOME="${XDG_CONFIG_HOME}/parallel"

	export PSQLRC="${XDG_CONFIG_HOME}/pg/psqlrc"
	export PSQL_HISTORY="${XDG_STATE_HOME}/psql_history"
	export PGPASSFILE="${XDG_CONFIG_HOME}/pg/pgpass"
	export PGSERVICEFILE="${XDG_CONFIG_HOME}/pg/pg_service.conf"

	export TEXMFHOME="${XDG_DATA_HOME}/texmf"
	export TEXMFVAR="${XDG_CACHE_HOME}/texlive/texmf-var"
	export TEXMFCONFIG="${XDG_CONFIG_HOME}/texlive/texmf-config"

	export SQLITE_HISTORY="${XDG_DATA_HOME}/sqlite_history"

	export CARGO_HOME="${XDG_DATA_HOME}/cargo"
fi

